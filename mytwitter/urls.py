"""mytwitter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from twitter import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home_view, name='home'),
    url(r'^register/$', views.register_view, name='register'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^user/(?P<user_name>[a-zA-Z@\d\.\+\-\_]+)/$', views.user_view, name='user'),
    url(r'^(?P<tweet_id>\d+)/$', views.details_view, name='details'),
    url(r'^add/$', views.add_view, name='add'),
    url(r'^preferences/$', views.preferences_view, name='preferences'),
    url(r'^(?P<operation>follow|unfollow)/user/(?P<user_name>[a-zA-Z@\d\.\+\-\_]+)/$', views.follow_view, name='change_follow'),
    url(r'^friends/$', views.friends_view, name='friends'),
    url(r'^ajax/tlitts_count/$', views.tlits_count, name='tlits_count'),
]
