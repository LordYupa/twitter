from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.views import logout as auth_logout, login as auth_login
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from twitter.forms import UserForm, AddTweetForm
from twitter.models import Tweet, Profile
from django.http import JsonResponse

def register_view(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            # Log in automatically after registration.
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserForm()
    return render(request, template_name='registration/register.html',
            context={'form': form})

def home_view(request):
    latest_tweets_list = Tweet.objects.order_by('-pub_date')[:10]
    return render(request, template_name='home.html',
            context={'latest_tweets_list': latest_tweets_list})

def details_view(request, tweet_id):
    tweet = get_object_or_404(Tweet, pk=tweet_id)
    return render(request, template_name='details.html',
            context={'tweet': tweet})

def login_view(request):
    if request.user.is_authenticated():
        return redirect('home')
    else:
        return auth_login(request)

def logout_view(request):
    return auth_logout(request, template_name='registration/logout.html')

@login_required(login_url='/login')
def add_view(request):
    if request.method == 'POST':
        form = AddTweetForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.pub_date = timezone.now()
            post.save()
            return redirect('home')
    else:
        form = AddTweetForm()
    return render(request, template_name='add.html', context={'form': form})

def user_view(request, user_name):
    account = get_object_or_404(User, username=user_name)
    user_tweets_list = Tweet.objects.filter(author=account) \
            .order_by('-pub_date')
    followers_list = account.profile.followers.all()
    followers_counter = len(followers_list)
    if request.user.is_authenticated():
        am_i_following = followers_list.filter(user=request.user).exists()
    else:
        am_i_following = False
    return render(request, template_name='user.html',
            context={'account': account,
                'user_tweets_list': user_tweets_list,
                'followers_list': followers_list[:10],
                'followers_counter': followers_counter,
                'am_i_following': am_i_following})

@login_required(login_url='/login')
def preferences_view(request):
    if request.method == 'POST':
        form = UserForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            # Log in automatically after registration.
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserForm(instance=request.user)
    return render(request, template_name='preferences.html',
            context={'form': form})

@login_required(login_url='/login')
def follow_view(request, operation, user_name):
    followed_user = get_object_or_404(User, username=user_name)
    if operation == 'follow':
        request.user.profile.following.add(followed_user.profile)
        following = True
    else:
        request.user.profile.following.remove(followed_user.profile)
        following = False
    return render(request, template_name='follow.html', context={
        'followed_user': followed_user, 'following': following})

@login_required(login_url='/login')
def friends_view(request):
    following_list = request.user.profile.following.all()
    following_counter = len(following_list)
    latest_friends_tweets_list = Tweet.objects.filter(author__profile__in= \
            following_list).order_by('-pub_date')[:10]
    return render(request, template_name='friends.html',
            context={'latest_friends_tweets_list': latest_friends_tweets_list,
                'following_list': following_list[:10],
                'following_counter' : following_counter})

@login_required(login_url='/login')
def tlits_count(request):
    data = {
        'counter': Tweet.objects.filter(author = request.user).count()
    }
    # return render(request, template_name='tlits_count.html',
    #         context={'tlits_count' : tlits_count})
    return JsonResponse(data)
