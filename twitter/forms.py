from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from twitter.models import Tweet
from django.utils import timezone

class UserForm(UserCreationForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=254, required=False,
            help_text='(Optional)')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1',
                'password2', )

class AddTweetForm(forms.ModelForm):
    tweet_text = forms.CharField(widget=forms.Textarea(attrs={'rows':5,
            'cols':40}), max_length=200)

    class Meta:
        model = Tweet
        fields = ('tweet_text', )