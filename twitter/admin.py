from django.contrib import admin
from twitter.models import Tweet

class TweetAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['tweet_text']}),
        ('User', {'fields': ['author']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    list_display = ('tweet_text', 'author', 'pub_date')
    search_fields = ['tweet_text']

admin.site.register(Tweet, TweetAdmin)