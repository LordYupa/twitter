from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save

class Tweet(models.Model):
    tweet_text = models.CharField(max_length=200)
    author = models.ForeignKey(User)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.tweet_text

class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    following = models.ManyToManyField('self', related_name='followers',
            symmetrical=False)

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)